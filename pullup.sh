#!/bin/bash
# fix for mk_arcade_joystick_rpi driver issue inherited from db9_gpio_rpi see https://github.com/marqs85/db9_gpio_rpi/issues/8
# RPi 4 and 400 need this tweak to speak to mk_arcade_joystick_rpi driver gpio connected arcade sticks and buttons as some inputs need an explicit pullup
# ensure that you're applying the pullups to the correct pins - this is for a standard deployment of the driver with two sets of buttons 
#
# Use:
# chmod pullup.sh +x
# add /path/to/pullup.sh to /etc/rc.local to load on boot

# Player 1 Joyport /dev/input/js0
raspi-gpio set 2 ip pu  #hotkey if you have one
raspi-gpio set 4 ip pu
raspi-gpio set 17 ip pu
raspi-gpio set 27 ip pu
raspi-gpio set 22 ip pu
raspi-gpio set 10 ip pu
raspi-gpio set 9 ip pu
raspi-gpio set 25 ip pu
raspi-gpio set 24 ip pu
raspi-gpio set 23 ip pu
raspi-gpio set 18 ip pu
raspi-gpio set 15 ip pu
raspi-gpio set 14 ip pu

# Player 2 Joyport /dev/input/js1
raspi-gpio set 3 ip pu  #hotkey 2 if you have one
raspi-gpio set 11 ip pu
raspi-gpio set 5 ip pu
raspi-gpio set 6 ip pu
raspi-gpio set 13 ip pu
raspi-gpio set 19 ip pu
raspi-gpio set 26 ip pu
raspi-gpio set 21 ip pu
raspi-gpio set 20 ip pu
raspi-gpio set 16 ip pu
raspi-gpio set 12 ip pu
raspi-gpio set 7 ip pu
raspi-gpio set 8 ip pu



