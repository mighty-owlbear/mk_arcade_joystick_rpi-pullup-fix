# mk_arcade_joystick_rpi pullup fix

Makes the [mk_arcade_joystick_rpi](https://gitlab.com/recalbox/mk_arcade_joystick_rpi) driver work properly on Raspberry Pi 4 and 400 running Rasbian based distros including RetroPie.

This fixes an mk_arcade_joystick_rpi [driver issue](https://github.com/marqs85/db9_gpio_rpi/issues/8) inherited from the db9_gpio_rpi driver on which it was originally based.

Raspberry Pi 4 and 400 running RetroPie and Raspbian need this tweak to speak to mk_arcade_joystick_rpi driver gpio connected arcade sticks and buttons as some inputs need an explicit pullup. The issue doesn't affect Recalbox, as it is not Raspbian based.

Ensure that you're applying the pullups to the correct pins - this script is for a standard deployment of the driver with two sets of buttons.

Once you've created the script - we put it in /home/pi/, you'll need to:

1. chmod pullup.sh +x
1. add /path/to/pullup.sh to /etc/rc.local to load on boot


----
Anti-deletion line, please ignore.
